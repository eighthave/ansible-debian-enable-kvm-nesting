#! /bin/bash

#
# The MIT License (MIT)
# 
# Copyright (c) 2018 Michael Pöhn
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to
# deal in the Software without restriction, including without limitation the
# rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
# sell copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
# IN THE SOFTWARE.
#

set -e

_print_help() {
    for ARG in "$*"; do
        if [ "$ARG" = "-h" -o "$ARG" = "--help" ]; then
            echo "A minimal script for enabling KVM nesting."
            echo "synopsis: enable_kvm_nesting.sh [-h|--help]"
            exit 0
        fi
    done
}

_test_debian_support() {
    if [ -z "`grep 'Debian GNU/Linux 12' /etc/issue`" \
	   -a -z "`grep 'Debian GNU/Linux 11' /etc/issue`" \
	   -a -z "`grep 'Debian GNU/Linux 10' /etc/issue`" ]; then
        echo "Error: This is not a Debian 10/11/12 based System."
        echo "       Can not enable VM nesting safely."
        exit 1
    fi
}

_test_root() {
    if [ `id --user` != "0" ]; then
        echo "Error: This script requires root privileges."
        echo "       Please run as root."
        exit 1
    fi
}

_grep_cpuinfo_vmx() {
    [ -n "`grep 'flags\s*:.*\svmx\s' /proc/cpuinfo`" ]
}

_grep_cpuinfo_svm() {
    [ -n "`grep 'flags\s*:.*\ssvm\s' /proc/cpuinfo`" ]
}


_kernel_module_in_use() {
    if _mod_enabled "$1"; then
        USE="`lsmod | grep \"^$1\" | awk '{print $3}'`"
        [ "$USE" != "0" ]
    else
        false
    fi
}

_mod_enabled() {
    [ -n "`lsmod | grep '^$1'`" ]
}

_nesting_enabled() {
    if [ -f /sys/module/"$1"/parameters/nested ]; then
        NESTED=`cat /sys/module/"$1"/parameters/nested`
        [ "$NESTED" = "Y" -o "$NESTED" = "1" ]
    else
        false
    fi
}

_human_readable() {
    if [ "$1" = "kvm_intel" ]; then
        echo 'Intel VT-x'
    elif [ "$1" = "kvm_amd" ]; then
        echo 'AMD SVM'
    fi
}

_enable_kvm_nesting() {

    MODULE="$1"

    if _nesting_enabled "$MODULE"; then
        echo "Nested KVM support for `_human_readable $MODULE` is already enabled."
        exit 0
    else
        if _kernel_module_in_use $MODULE; then
            echo "Error: `_human_readable $MODULE` virutalization is currently in use."
            echo "(If you've got VMs running, you'll need to shut them down.)"
            exit 1
        fi

        if [ "$MODULE" = "kvm_amd" ]; then
            echo "options kvm-amd nested=1" > /etc/modprobe.d/kvm-amd.conf
        elif [ "$MODULE" = "kvm_intel" ]; then
            echo "options kvm-intel nested=1" > /etc/modprobe.d/kvm-intel.conf
        fi

        rmmod $MODULE
        modprobe $MODULE

        if _nesting_enabled "$MODULE"; then
            echo "Sucessfully enabled KVM nesting support for `_human_readable $MODULE`."
        else
            echo "Error: Failed to enable KVM nesting support for `_human_readable $MODULE`"
        fi
    fi
    
}

_print_help "$*"
_test_debian_support
_test_root
if _grep_cpuinfo_svm; then
    _enable_kvm_nesting "kvm_amd"
elif _grep_cpuinfo_vmx; then 
    _enable_kvm_nesting "kvm_intel"
else
    echo "Error: Could detect neiter AMD SVM nor Intel VT-x support."
    echo "(You might need to enable virtualization in BIOS/UEFI.)"
    exit 1
fi
